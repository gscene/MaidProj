﻿#ifndef OPTIONEDITOR_H
#define OPTIONEDITOR_H

#include <QDialog>
#include <QStringListModel>
#include <QMessageBox>
#include "optionhandler.h"

#define LST_PROVIDER "../etc/info/td_provider.json"
#define LST_CONTRACTOR "../etc/info/td_contractor.json"
#define LST_RECEIVER "../etc/info/td_receiver.json"
#define LST_EXPRESS "../etc/info/td_express.json"
#define LST_TRANSACTOR "../etc/info/td_transactor.json"
#define LST_LOGISTICS "../etc/info/td_logistics.json"
#define OPT_EDIT QStringLiteral("编辑...")
#define OPT_TEXT QStringLiteral("替换文本")

namespace Ui {
class OptionEditor;
}

enum OptionType
{
    O_PROVIDER,
    O_CONTRACTOR,
    O_RECEIVER,
    O_EXPRESS,
    O_TRANSACTOR,
    O_LOGISTICS
};

class OptionEditor : public QDialog
{
    Q_OBJECT

public:
    explicit OptionEditor(QWidget *parent = 0);
    ~OptionEditor();

    bool detect(const OptionType &type);

signals:
    void reload(const OptionType &type);

private slots:
    void on_btn_add_clicked();

    void on_btn_del_clicked();

    void on_btn_save_clicked();

private:
    Ui::OptionEditor *ui;

    OptionHandler option;
    QString _file;
    int saveFlag;       // 0 未修改，-1 未保存，1已保存

    OptionType _type;
    QStringListModel *model;
};

#endif // OPTIONEDITOR_H
