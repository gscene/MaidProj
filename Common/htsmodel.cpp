﻿#include "htsmodel.h"

HTSModel::HTSModel(QObject *parent) : QObject(parent)
{
    _model=new QStandardItemModel(this);
}

bool HTSModel::load(const QString &fileName)
{
    QFile file(fileName);
    if(file.exists() && file.open(QFile::ReadOnly | QFile::Text))
    {
        QJsonDocument doc;
        QFileInfo fi(fileName);
        if(fi.suffix() == "json")
            doc=QJsonDocument::fromJson(file.readAll());
        else
            doc=QJsonDocument::fromBinaryData(file.readAll());

        if(!doc.isEmpty() && doc.isObject())
        {
            QJsonObject obj=doc.object();
            _title=obj.value("title").toString();
            _jsonArray=obj.value("content").toArray();

            for(int i=0;i<_jsonArray.size();i++)
            {
                QJsonObject obj=_jsonArray[i].toObject();
                _head=obj.value(HT_HEAD).toString();
                _tail=obj.value(HT_TAIL).toString();
                _headList.append(_head);

                QList<QStandardItem *> item;
                item.append(new QStandardItem(_head));
                item.append(new QStandardItem(_tail));
                _model->appendRow(item);
            }
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

QStandardItemModel* HTSModel::getModel() const
{
    return _model;
}

