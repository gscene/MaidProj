﻿#include "optionhandler.h"

bool OptionHandler::load(const QString &fileName)
{
    QFile file(fileName);
    if(file.exists() && file.open(QFile::ReadOnly | QFile::Text))
    {
        QJsonDocument doc=QJsonDocument::fromJson(file.readAll());
        if(!doc.isEmpty() && doc.isArray())
        {
            _array=doc.array();
            if(!_list.isEmpty())
                _list.clear();
            for(int i=0;i<_array.size();i++)
            {
                _list.append(_array[i].toString());
            }
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

bool OptionHandler::save(const QString &fileName)
{
    QFile file(fileName);
    if(file.open(QFile::WriteOnly))
    {
        QJsonDocument doc(_array);
        file.write(doc.toJson());
        file.close();
        return true;
    }
    else
        return false;
}

void OptionHandler::setStringList(const QStringList &list)
{
    _array=QJsonArray::fromStringList(list);
}

QStringList OptionHandler::getStringList() const
{
    return _list;
}
