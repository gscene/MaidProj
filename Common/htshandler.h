﻿#ifndef HTSHANDLER_H
#define HTSHANDLER_H

#include <QString>
#include <QStringList>
#include <QFile>
#include <QFileInfo>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QHash>

#define HT_HEAD "name"
#define HT_TAIL "url"

class HTSHandler
{
public:
    bool load(const QString &fileName);
    bool save(const QString &fileName);

    QString getFileName() const;
    void setFileName(const QString &fileName);

    QString getTitle() const;
    void setTitle(const QString &title);

    QStringList getHeadList() const;
    QHash<QString,QString> getHashTable() const;

    QJsonArray getJsonArray() const;
    void setJsonArray(const QJsonArray &array);

    void append(const QString &head, const QString &tail);
    void remove(int pos);

protected:
    QString _file;
    QString _title;
    QJsonArray _jsonArray;
    QString _head,_tail;
    QStringList _headList;
    QHash<QString,QString> _hashTable;
};

#endif // HTSHANDLER_H
