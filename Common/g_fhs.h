﻿#ifndef G_FHS_H
#define G_FHS_H

#define FHS_BIN "."
#define FHS_ETC "../etc"
#define FHS_LIB "../lib"
#define FHS_SHARE "../share"

#define FHS_OPT "../opt"
#define FHS_TMP "../tmp"

#define FHS_VAR "../var"
#define FHS_RUN "../var/run"
#define FHS_LOG "../var/log"
#define FHS_INFO "../var/info"

#endif // G_FHS_H
