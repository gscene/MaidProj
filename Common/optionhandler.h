﻿#ifndef OPTIONHANDLER_H
#define OPTIONHANDLER_H

#include <QFile>
#include <QByteArray>
#include <QString>
#include <QStringList>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

class OptionHandler
{
public:
    bool load(const QString &fileName);
    bool save(const QString &fileName);

    void setStringList(const QStringList &list);
    QStringList getStringList() const;

private:
    QStringList _list;
    QJsonArray _array;
};

#endif // OPTIONHANDLER_H
