﻿#ifndef G_DEF_H
#define G_DEF_H

/*
 *  统一程序接口定义
 *  SP_VER 数字版本号，必需
 *  SP_NAME 字符版本号，必需
 *  APP_VER 可视字符版本号，非必需
 *
 * SP_TYPE 说明
 *      CORE 1
 *      SUPPORT 3
 *      SHELL 5
 *
 * 版本号说明
 * 结构： 功能号（2位） + 修订号（2位）
*/

#include "g_fhs.h"

#define T_NAME "SP_NAME"
#define T_TYPE "SP_TYPE"
#define T_VER "SP_VER"

#define T_CORE 1
#define T_SUPPORT 3
#define T_EXTRA 4
#define T_SHELL 5



#endif // G_DEF_H
