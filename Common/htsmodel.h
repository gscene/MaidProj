﻿#ifndef HTSMODEL_H
#define HTSMODEL_H

#include <QObject>
#include <QList>
#include <QStandardItem>
#include <QStandardItemModel>

#include "htshandler.h"

class HTSModel : public QObject, public HTSHandler
{
    Q_OBJECT

public:
    explicit HTSModel(QObject *parent = 0);
    bool load(const QString &fileName);
    QStandardItemModel* getModel() const;

private:
    QStandardItemModel *_model;
};

#endif // HTSMODEL_H
