﻿#ifndef G_FUNCTION
#define G_FUNCTION

#include <cstring>
#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include "g_ver.h"

static void sp_createRunFile(const QString &appName)
{
    QJsonObject obj;
    obj.insert(T_NAME,SP_NAME);
    obj.insert(T_TYPE,SP_TYPE);
    obj.insert(T_VER,SP_VER);

    QString fileName=FHS_RUN + QString("/") + appName + QString(".json");

    QFile file(fileName);
    if(file.open(QFile::WriteOnly | QFile::Text))
    {
        QJsonDocument doc(obj);
        file.write(doc.toJson());
        file.close();
    }
}

#endif // G_FUNCTION

