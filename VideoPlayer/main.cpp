﻿#include "videoplayer.h"
#include <QtSingleApplication>
#include "g_function.h"

int main(int argc, char *argv[])
{
    QtSingleApplication  a(SP_NAME,argc, argv);
    QtAV::Widgets::registerRenderers();

    if(argc == 2)
    {
        if(strcmp(argv[1],"-v") == 0)
        {
            sp_createRunFile(a.applicationName());
            return EXIT_SUCCESS;
        }
    }

    if(a.isRunning())
    {
        QMessageBox::critical(0,"ERROR",QStringLiteral("已经有一个实例在运行！"));
        return EXIT_SUCCESS;
    }

    VideoPlayer w;
    w.resize(960,540);
    w.show();

    return a.exec();
}
