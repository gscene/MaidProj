﻿#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QtAV>
#include <QtAVWidgets>
#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QLayout>
#include <QMessageBox>
#include <QFileDialog>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QStandardPaths>

using namespace QtAV;

class VideoPlayer : public QWidget
{
    Q_OBJECT

public:
    VideoPlayer(QWidget *parent = 0);
    ~VideoPlayer();

    void mouseReleaseEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *event);

    void open();
    void seek(int);
    void detectState();
    void stop();

private slots:
    void updateSlider();

private:
    VideoOutput *m_vo;
    AVPlayer *m_player;
    QSlider *m_slider;
    QString path;
};

#endif // VIDEOPLAYER_H
