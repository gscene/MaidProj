#-------------------------------------------------
#
# Project created by QtCreator 2015-09-14T18:43:15
#
#-------------------------------------------------

QT       += core gui av avwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoPlayer
TEMPLATE = app

INCLUDEPATH += ../Common

include(../qtsingleapplication/src/qtsingleapplication.pri)

SOURCES += main.cpp\
        videoplayer.cpp

HEADERS  += videoplayer.h \
    ../Common/g_def.h \
    ../Common/g_fhs.h \
    ../Common/g_function.h \
    g_ver.h

RESOURCES += \
    res.qrc
