#-------------------------------------------------
#
# Project created by QtCreator 2015-05-21T20:45:11
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = DesktopMaid
TEMPLATE = app

INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/function
INCLUDEPATH += $$PWD/module
INCLUDEPATH += ../Common

include(../qtsingleapplication/src/qtsingleapplication.pri)

SOURCES += main.cpp\
    facade.cpp \
    module/t_dialog.cpp \
    module/torus.cpp \
    function/sp_env.cpp \
    function/sprite.cpp \
    function/master.cpp \
    function/sp_heart.cpp \
    module/searchbox.cpp \
    t_facade.cpp \
    module/note.cpp \
    module/note2.cpp \
    ../Common/htshandler.cpp \
    ../Common/htsmodel.cpp \
    module/favoriteeditor.cpp \
    figure.cpp \
    module/menueditor.cpp

HEADERS  += facade.h \
    figure.h \
    module/t_dialog.h \
    module/torus.h \
    function/sp_env.h \
    function/sprite.h \
    function/m_expr.h \
    function/m_fhs.h \
    function/m_heart.h \
    function/master.h \
    function/sp_heart.h \
    module/searchbox.h \
    t_facade.h \
    module/note.h \
    module/note2.h \
    ../Common/htshandler.h \
    ../Common/htsmodel.h \
    module/favoriteeditor.h \
    g_ver.h \
    ../Common/g_function.h \
    module/menueditor.h \
    ../Common/g_def.h \
    ../Common/g_fhs.h

FORMS    += facade.ui \
    module/note.ui \
    module/note2.ui \
    module/torus.ui \
    module/searchbox.ui \
    module/favoriteeditor.ui \
    module/menueditor.ui

RC_ICONS = ico.ico

RESOURCES += \
    res.qrc
