﻿#ifndef FIGURE
#define FIGURE

#include "torus.h"
#include "sprite.h"

#include "htshandler.h"
#include "searchbox.h"

#include "note.h"
#include "note2.h"

#include "favoriteeditor.h"
#include "menueditor.h"

struct MenuFlag
{
    MenuFlag();

    bool getFlag(const MenuType &type);
    void setFlag(const MenuType &type, bool flag);

    bool banLocation; // 默认0 显示，1隐藏
    bool banTool;
    bool banGroup;
    bool banProps;
    bool banFavorite;
    bool banCommand;
};

#endif // FIGURE

