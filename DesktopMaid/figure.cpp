﻿#include "figure.h"

MenuFlag::MenuFlag()
{
    QFile file(MENU_FILE);
    if(!file.exists())
    {
        banLocation=false;
        banTool=false;
        banGroup=false;
        banProps=false;
        banFavorite=false;
        banCommand=true;
    }
    else
    {
        banLocation=getFlag(M_LOCATION);
        banTool=getFlag(M_TOOL);
        banGroup=getFlag(M_GROUP);
        banProps=getFlag(M_PROPS);
        banFavorite=getFlag(M_FAVORITE);
        banCommand=getFlag(M_COMMAND);
    }
}

bool MenuFlag::getFlag(const MenuType &type)
{
    bool flag,ok;
    int ret;
    QSettings ini(MENU_FILE,QSettings::IniFormat);

    switch (type) {
    case M_LOCATION:
        ret=ini.value("menu/banLocation").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    case M_TOOL:
        ret=ini.value("menu/banTool").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    case M_GROUP:
        ret=ini.value("menu/banGroup").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    case M_PROPS:
        ret=ini.value("menu/banProps").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    case M_FAVORITE:
        ret=ini.value("menu/banFavorite").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    case M_COMMAND:
        ret=ini.value("menu/banCommand").toInt(&ok);
        if(ok)
        {
            if(ret == 1)
                flag=true;
            else
                flag=false;
        }
        else
            flag=false;
        break;
    default:
        flag=false;
        break;
    }
    return flag;
}

void MenuFlag::setFlag(const MenuType &type, bool flag)
{
    int flagNo=0;
    if(flag)
        flagNo=1;

    QSettings ini(MENU_FILE,QSettings::IniFormat);
    switch (type) {
    case M_LOCATION:
        ini.setValue("menu/banLocation",flagNo);
        break;
    case M_TOOL:
        ini.setValue("menu/banTool",flagNo);
        break;
    case M_GROUP:
        ini.setValue("menu/banGroup",flagNo);
        break;
    case M_PROPS:
        ini.setValue("menu/banProps",flagNo);
        break;
    case M_FAVORITE:
        ini.setValue("menu/banFavorite",flagNo);
        break;
    case M_COMMAND:
        ini.setValue("menu/banCommand",flagNo);
        break;
    }
}
