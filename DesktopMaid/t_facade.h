﻿#ifndef T_FACADE_H
#define T_FACADE_H

#include"sp_env.h"

#define DEF_WID 360
#define DEF_HEI 450

/*
 *  统一程序接口定义
 *  SP_VER 数字版本号，必需
 *  SP_NAME 字符版本号，必需
 *  APP_VER 可视字符版本号，非必需
 *
 * 版本号说明
 * 结构： 功能号（2位） + 修订号（2位）
*/

class TransFacade : public QWidget
{
    Q_OBJECT

public:
    explicit TransFacade(QWidget *parent = 0);

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void detectVisible();
    void createTray();
    void moveDefPos();
    void setDefPos();

protected slots:
    void trayActivated(QSystemTrayIcon::ActivationReason);

protected:
    QPoint winPos;
    QPoint mosPos;
    QPoint dPos;
    QSystemTrayIcon *tray;
    QMenu *tray_Menu;
    QAction *tray_exitAction;

    int d_width;
    int d_height;
    QPoint defPos;
    QPoint curPos;
};

#endif // T_FACADE_H
