﻿#include "facade.h"
#include "ui_facade.h"

Facade::Facade(QWidget *parent) :
    TransFacade(parent),
    ui(new Ui::Facade)
{
    ui->setupUi(this);

    s02;
    startUp();
}

Facade::~Facade()
{
    sp.sleep();
    delete ui;
}

void Facade::startUp()
{
    QDir dir(Path::document + APP_ROOT);
    if(!dir.exists())
        dir.mkpath(Path::document + APP_ROOT);

    menu=nullptr;
    m_location=nullptr;
    m_command=nullptr;
    m_tool=nullptr;
    m_props=nullptr;
    m_note=nullptr;
    m_option=nullptr;
    m_menuDetect=nullptr;
    m_favorite=nullptr;

    note=nullptr;
    note2=nullptr;
    searchBox=nullptr;
    tor=new Torus(this);

    createMenuAll();

    sp.wakeUp();
    initSpTimer();
    timeSection_Greet();
}

void Facade::contextMenuEvent(QContextMenuEvent *)
{
    menu->exec(QCursor::pos());
}

void Facade::moveEvent(QMoveEvent *)
{
    curPos=this->pos();

    if(curPos.x() >= d_width - 32 || curPos.x() <= -96)
    {
        moveDefPos();
    }
    else if(curPos.y() >= d_height -32 || curPos.y() <= -96)
    {
        moveDefPos();
    }
}

void Facade::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::AltModifier && event->key()==Qt::Key_1)
        showNote();
    else if(event->modifiers() == Qt::AltModifier && event->key()==Qt::Key_2)
        showNote2();
    else if(event->modifiers() == Qt::AltModifier && event->key()==Qt::Key_F2)
        showSearchBox();
    else
    {
        switch(event->key())
        {
        case Qt::Key_Home:
            moveDefPos();
            break;
        case Qt::Key_F1:
            QDesktopServices::openUrl(QUrl::fromLocalFile(README));
            break;
        }
    }
}

void Facade::delay(int sec)
{
    time.start();
    while(time.elapsed()<sec*1000)
        qApp->processEvents();
}

void Facade::mouseDoubleClickEvent(QMouseEvent *)
{
    s04;
    say(QStringLiteral("主人，有什么事吗？"),6);
    delay(6);
    s02;
}

void Facade::timeSection_Greet()
{
    hour=QTime::currentTime().hour();

    if(hour >=0 && hour <=6)
        word= QStringLiteral("早上好，主人") ;
    else if(hour >=7 && hour <= 10 )
        word= QStringLiteral("上午好，主人");
    else if(hour >= 11 && hour <=12)
        word= QStringLiteral("中午好，主人");
    else if(hour >=13 && hour <=17)
        word=  QStringLiteral("下午好，主人");
    else if(hour >=18 && hour <=23)
        word= QStringLiteral("晚上好，主人");

    s02;
    say(word);

    if(hour >=20 || hour <=2)
        QTimer::singleShot(1000*6 ,this,&Facade::timeSection_Remind);
    else
        QTimer::singleShot(1000*6 ,this,&Facade::directSay);
}

void Facade::timeSection_Remind()
{
    if(hour >= 0 && hour <= 2)
        word= QStringLiteral("凌晨了，早点睡吧，再不睡就天亮了！");
    else if(hour >= 22)
        word= QStringLiteral("深夜了，早点睡吧。");
    else if(hour >= 20 && hour <=21)
        word=QStringLiteral("夜晚了，记得早点休息哦。");

    s04;
    say(word);
}

void Facade::directSay()
{
    word=QStringLiteral("祝您玩的开心，我会默默看着您的。");
    s02;
    say(word);
}

void Facade::initSpTimer()
{
    QTimer::singleShot(1000 * 10 ,this,&Facade::sessionTick);

    liveTimer=new QTimer(this);
    connect(liveTimer,&QTimer::timeout,this,&Facade::liveTick);
    liveTimer->start(1000 * 900);
}

void Facade::sessionTick()
{
    sp.sessionUpdate();
}

void Facade::liveTick()
{
    sp.liveUpdate();
}

void Facade::createMenuAll()
{
    menu=new QMenu(this);
    m_menuDetect=new QMenu(QStringLiteral("菜单设置"),this);

    createLocationMenu();
    createToolMenu();
    createCommandMenu();
    createPropsMenu();
    createOptionMenu();
    createFavoriteMenu();

    if(!word.isEmpty())
        say(word,6);
}

void Facade::reloadMenuAll()
{
    word.clear();

    menu->clear();
    m_menuDetect->clear();

    createLocationMenu();
    createToolMenu();
    createCommandMenu();
    createPropsMenu();
    createOptionMenu();
    createFavoriteMenu();

    if(!word.isEmpty())
        say(word,6);
}

void Facade::createLocationMenu()
{
    if(menuFlag.banLocation)
    {
        detectLocationAction=m_menuDetect->addAction(QStringLiteral("启用位置菜单"));
        connect(detectLocationAction,&QAction::triggered,this,&Facade::detectLocationMenu);
        return;
    }
    else
    {
        HTSHandler handler;
        if(!handler.load(MENU_LOCATION))
            m_location=menu->addMenu(QStringLiteral("位置"));
        else
        {
            m_location=menu->addMenu(handler.getTitle());

            locationMenuItems=handler.getHashTable();
            QHash<QString,QString>::const_iterator it;
            for(it=locationMenuItems.begin();it!=locationMenuItems.end();it++)
            {
                m_location->addAction(it.key());
            }
            connect(m_location,SIGNAL(triggered(QAction*)),this,SLOT(processLocationAction(QAction*)));
            m_location->addSeparator();
        }

        editLocationMenuAction=m_location->addAction(QStringLiteral("编辑..."));
        connect(editLocationMenuAction,&QAction::triggered,this,&Facade::editLocationMenu);

        detectLocationAction=m_menuDetect->addAction(QStringLiteral("禁用位置菜单"));
        connect(detectLocationAction,&QAction::triggered,this,&Facade::detectLocationMenu);
    }
}

void Facade::createPropsMenu()
{
    if(menuFlag.banProps)
    {
        detectPropsMenuAction=m_menuDetect->addAction(QStringLiteral("启用组件菜单"));
        connect(detectPropsMenuAction,&QAction::triggered,this,&Facade::detectPropsMenu);
        return;
    }
    else
    {
        m_props=menu->addMenu(QStringLiteral("组件"));
        m_note=m_props->addMenu(QStringLiteral("速记"));

        showNoteAction=m_note->addAction(QStringLiteral("速记1 (Alt+1)"));
        connect(showNoteAction,&QAction::triggered,this,&Facade::showNote);

        showNote2Action=m_note->addAction(QStringLiteral("速记2 (Alt+2)"));
        connect(showNote2Action,&QAction::triggered,this,&Facade::showNote2);

        showSearchBoxAction=m_props->addAction(QStringLiteral("速搜 (Alt+F2)"));
        connect(showSearchBoxAction,&QAction::triggered,this,&Facade::showSearchBox);


        if(!propsList.isEmpty())
            propsList.clear();
        propsList.append(showNoteAction);
        propsList.append(showNote2Action);
        propsList.append(showSearchBoxAction);

        HTSHandler handler;
        if(handler.load(MENU_PROPS))
        {
            propsMenuItems=handler.getHashTable();
            QHash<QString,QString>::const_iterator it;
            for(it=propsMenuItems.begin();it!=propsMenuItems.end();it++)
            {
                m_props->addAction(it.key());
            }
            connect(m_props,SIGNAL(triggered(QAction*)),this,SLOT(processPropsAction(QAction*)));
        }

        m_props->addSeparator();
        editPropsMenuAction=m_props->addAction(QStringLiteral("编辑..."));
        connect(editPropsMenuAction,&QAction::triggered,this,&Facade::editPropsMenu);

        detectPropsMenuAction=m_menuDetect->addAction(QStringLiteral("禁用组件菜单"));
        connect(detectPropsMenuAction,&QAction::triggered,this,&Facade::detectPropsMenu);
    }
}

void Facade::createOptionMenu()
{
    m_option=menu->addMenu(QStringLiteral("选项"));

    m_option->addMenu(m_menuDetect);

    quitAction=m_option->addAction(QStringLiteral("退出"));
    connect(quitAction,&QAction::triggered,this,&QApplication::quit);
}

void Facade::createCommandMenu()
{
    if(menuFlag.banCommand)
    {
        detectCommandMenuAction=m_menuDetect->addAction(QStringLiteral("启用命令菜单"));
        connect(detectCommandMenuAction,&QAction::triggered,this,&Facade::detectCommandMenu);
        return;
    }
    else
    {
        HTSHandler handler;
        if(!handler.load(MENU_CMD))
            m_command=menu->addMenu(QStringLiteral("命令"));
        else
        {
            m_command=menu->addMenu(handler.getTitle());

            commandMenuItems=handler.getHashTable();
            QHash<QString,QString>::const_iterator it;
            for(it=commandMenuItems.begin();it!=commandMenuItems.end();it++)
            {
                m_command->addAction(it.key());
            }
            connect(m_command,SIGNAL(triggered(QAction*)),this,SLOT(processCommandAction(QAction*)));
            m_command->addSeparator();
        }

        editCommandMenuAction=m_command->addAction(QStringLiteral("编辑..."));
        connect(editCommandMenuAction,&QAction::triggered,this,&Facade::editCommandMenu);

        detectCommandMenuAction=m_menuDetect->addAction(QStringLiteral("禁用命令菜单"));
        connect(detectCommandMenuAction,&QAction::triggered,this,&Facade::detectCommandMenu);
    }
}

void Facade::createToolMenu()
{
    if(menuFlag.banTool)
    {
        detectToolMenuAction=m_menuDetect->addAction(QStringLiteral("启用工具菜单"));
        connect(detectToolMenuAction,&QAction::triggered,this,&Facade::detectToolMenu);
        return;
    }
    else
    {
        HTSHandler handler;
        if(!handler.load(MENU_TOOL))
            m_tool=menu->addMenu(QStringLiteral("工具"));
        else
        {
            m_tool=menu->addMenu(handler.getTitle());

            toolMenuItems=handler.getHashTable();
            QHash<QString,QString>::const_iterator it;
            for(it=toolMenuItems.begin();it!=toolMenuItems.end();it++)
            {
                m_tool->addAction(it.key());
            }
            connect(m_tool,SIGNAL(triggered(QAction*)),this,SLOT(processToolAction(QAction*)));
            m_tool->addSeparator();
        }
        editToolMenuAction=m_tool->addAction(QStringLiteral("编辑..."));
        connect(editToolMenuAction,&QAction::triggered,this,&Facade::editToolMenu);

        detectToolMenuAction=m_menuDetect->addAction(QStringLiteral("禁用工具菜单"));
        connect(detectToolMenuAction,&QAction::triggered,this,&Facade::detectToolMenu);
    }
}

void Facade::editCommandMenu()
{
    MenuEditor meditor;
    meditor.move(400,150);
    if(meditor.detect(M_COMMAND))
    {
        connect(&meditor,SIGNAL(reload(MenuType)),this,SLOT(processMenuReload(MenuType)));
        meditor.exec();
    }
    else
        say(QStringLiteral("无法加载命令菜单！"));
}

void Facade::editLocationMenu()
{
    MenuEditor meditor;
    meditor.move(400,150);
    if(meditor.detect(M_LOCATION))
    {
        connect(&meditor,SIGNAL(reload(MenuType)),this,SLOT(processMenuReload(MenuType)));
        meditor.exec();
    }
    else
        say(QStringLiteral("无法加载位置菜单！"));
}

void Facade::editPropsMenu()
{
    MenuEditor meditor;
    meditor.move(400,150);
    if(meditor.detect(M_PROPS))
    {
        connect(&meditor,SIGNAL(reload(MenuType)),this,SLOT(processMenuReload(MenuType)));
        meditor.exec();
    }
    else
        say(QStringLiteral("无法加载组件菜单！"));
}

void Facade::editToolMenu()
{
    MenuEditor meditor;
    meditor.move(400,150);
    if(meditor.detect(M_TOOL))
    {
        connect(&meditor,SIGNAL(reload(MenuType)),this,SLOT(processMenuReload(MenuType)));
        meditor.exec();
    }
    else
        say(QStringLiteral("无法加载工具菜单！"));
}

void Facade::createFavoriteMenu()
{
    if(menuFlag.banFavorite)
    {
        detectFavoriteMenuAction=m_menuDetect->addAction(QStringLiteral("启用收藏夹"));
        connect(detectFavoriteMenuAction,&QAction::triggered,this,&Facade::detectFavoriteMenu);
        return;
    }
    else
    {
        HTSHandler handler;
        if(!handler.load(Path::document + FAV_FILE))
            m_favorite=menu->addMenu(QStringLiteral("收藏夹"));
        else
        {
            m_favorite=menu->addMenu(handler.getTitle());
            favoriteMenuItems=handler.getHashTable();
            QHash<QString,QString>::const_iterator it;
            for(it=favoriteMenuItems.begin();it!=favoriteMenuItems.end();it++)
            {
                m_favorite->addAction(it.key());
            }
            connect(m_favorite,SIGNAL(triggered(QAction*)),this,SLOT(processFavoriteAction(QAction*)));

            m_favorite->addSeparator();
        }
        editFavoriteMenuAction=m_favorite->addAction(QStringLiteral("编辑..."));
        connect(editFavoriteMenuAction,&QAction::triggered,this,&Facade::editFavoriteMenu);

        detectFavoriteMenuAction=m_menuDetect->addAction(QStringLiteral("禁用收藏夹"));
        connect(detectFavoriteMenuAction,&QAction::triggered,this,&Facade::detectFavoriteMenu);
    }
}

void Facade::editFavoriteMenu()
{
    FavoriteEditor favEditor;
    favEditor.move(400,150);
    if(favEditor.detect(Path::document + FAV_FILE))
    {
        connect(&favEditor,&FavoriteEditor::reload,this,&Facade::reloadFavoriteMenu);
        favEditor.exec();
    }
    else
        say(QStringLiteral("无法加载收藏夹！"));
}

void Facade::reloadFavoriteMenu()
{
    if(m_favorite == nullptr)
        return;

    m_favorite->clear();
    m_favorite->disconnect();

    HTSHandler handler;
    if(handler.load(Path::document + FAV_FILE))
    {
        favoriteMenuItems=handler.getHashTable();
        QHash<QString,QString>::const_iterator it;
        for(it=favoriteMenuItems.begin();it!=favoriteMenuItems.end();it++)
        {
            m_favorite->addAction(it.key());
        }
        connect(m_favorite,SIGNAL(triggered(QAction*)),this,SLOT(processFavoriteAction(QAction*)));
        m_favorite->addSeparator();
        say(QStringLiteral("收藏夹已刷新"));
    }
    else
        say(QStringLiteral("无法加载收藏夹！"));

    editFavoriteMenuAction=m_favorite->addAction(QStringLiteral("编辑"));
    connect(editFavoriteMenuAction,&QAction::triggered,this,&Facade::editFavoriteMenu);
}

void Facade::reloadPropsMenu()
{
    if(m_props == nullptr)
        return;

    m_props->clear();
    m_props->disconnect();

    m_note=m_props->addMenu(QStringLiteral("速记"));
    showNoteAction=m_note->addAction(QStringLiteral("速记1 (Alt+1)"));
    connect(showNoteAction,&QAction::triggered,this,&Facade::showNote);

    showNote2Action=m_note->addAction(QStringLiteral("速记2 (Alt+2)"));
    connect(showNote2Action,&QAction::triggered,this,&Facade::showNote2);

    showSearchBoxAction=m_props->addAction(QStringLiteral("速搜 (Alt+F2)"));
    connect(showSearchBoxAction,&QAction::triggered,this,&Facade::showSearchBox);

    if(!propsList.isEmpty())
        propsList.clear();
    propsList.append(showNoteAction);
    propsList.append(showNote2Action);
    propsList.append(showSearchBoxAction);

    HTSHandler handler;
    if(handler.load(MENU_PROPS))
    {
        propsMenuItems=handler.getHashTable();
        QHash<QString,QString>::const_iterator it;
        for(it=propsMenuItems.begin();it!=propsMenuItems.end();it++)
        {
            m_props->addAction(it.key());
        }
        connect(m_props,SIGNAL(triggered(QAction*)),this,SLOT(processPropsAction(QAction*)));
        m_props->addSeparator();
        say(QStringLiteral("组件菜单已刷新"));
    }
    else
        say(QStringLiteral("无法刷新组件菜单！"));

    editPropsMenuAction=m_props->addAction(QStringLiteral("编辑..."));
    connect(editPropsMenuAction,&QAction::triggered,this,&Facade::editPropsMenu);
}

void Facade::reloadCommandMenu()
{
    if(m_command == nullptr)
        return;

    m_command->clear();
    m_command->disconnect();

    HTSHandler handler;
    if(handler.load(MENU_CMD))
    {
        commandMenuItems=handler.getHashTable();
        QHash<QString,QString>::const_iterator it;
        for(it=commandMenuItems.begin();it!=commandMenuItems.end();it++)
        {
            m_command->addAction(it.key());
        }
        connect(m_command,SIGNAL(triggered(QAction*)),this,SLOT(processCommandAction(QAction*)));
        m_command->addSeparator();
        say(QStringLiteral("命令菜单已刷新"));
    }
    else
        say(QStringLiteral("无法刷新命令菜单！"));

    editCommandMenuAction=m_command->addAction(QStringLiteral("编辑..."));
    connect(editCommandMenuAction,&QAction::triggered,this,&Facade::editCommandMenu);
}

void Facade::reloadToolMenu()
{
    if(m_tool == nullptr)
        return;

    m_tool->clear();
    m_tool->disconnect();

    HTSHandler handler;
    if(handler.load(MENU_TOOL))
    {
        toolMenuItems=handler.getHashTable();
        QHash<QString,QString>::const_iterator it;
        for(it=toolMenuItems.begin();it!=toolMenuItems.end();it++)
        {
            m_tool->addAction(it.key());
        }
        connect(m_tool,SIGNAL(triggered(QAction*)),this,SLOT(processToolAction(QAction*)));
        m_tool->addSeparator();
        say(QStringLiteral("工具菜单已刷新"));
    }
    else
        say(QStringLiteral("无法刷新工具菜单！"));

    editToolMenuAction=m_tool->addAction(QStringLiteral("编辑..."));
    connect(editToolMenuAction,&QAction::triggered,this,&Facade::editToolMenu);
}

void Facade::reloadLocationMenu()
{
    if(m_location == nullptr)
        return;

    m_location->clear();
    m_location->disconnect();

    HTSHandler handler;
    if(handler.load(MENU_LOCATION))
    {
        locationMenuItems=handler.getHashTable();
        QHash<QString,QString>::const_iterator it;
        for(it=locationMenuItems.begin();it!=locationMenuItems.end();it++)
        {
            m_location->addAction(it.key());
        }
        connect(m_location,SIGNAL(triggered(QAction*)),this,SLOT(processLocationAction(QAction*)));
        m_location->addSeparator();
        say(QStringLiteral("位置菜单已刷新"));
    }
    else
        say(QStringLiteral("无法刷新位置菜单！"));

    editLocationMenuAction=m_location->addAction(QStringLiteral("编辑..."));
    connect(editLocationMenuAction,&QAction::triggered,this,&Facade::editLocationMenu);
}

void Facade::detectCommandMenu()
{
    if(menuFlag.banCommand)
    {
        menuFlag.banCommand=false;
        menuFlag.setFlag(M_COMMAND,false);
        say(QStringLiteral("命令菜单已经启用"));
    }
    else
    {
        menuFlag.banCommand=true;
        menuFlag.setFlag(M_COMMAND,true);
        say(QStringLiteral("命令菜单已经禁用"));
    }
    reloadMenuAll();
}

void Facade::detectFavoriteMenu()
{
    if(menuFlag.banFavorite)
    {
        menuFlag.banFavorite=false;
        menuFlag.setFlag(M_FAVORITE,false);
        say(QStringLiteral("收藏夹已经启用"));
    }
    else
    {
        menuFlag.banFavorite=true;
        menuFlag.setFlag(M_FAVORITE,true);
        say(QStringLiteral("收藏夹已经禁用"));
    }
    reloadMenuAll();
}

void Facade::detectLocationMenu()
{
    if(menuFlag.banLocation)
    {
        menuFlag.banLocation=false;
        menuFlag.setFlag(M_LOCATION,false);
        say(QStringLiteral("位置菜单已经启用"));
    }
    else
    {
        menuFlag.banLocation=true;
        menuFlag.setFlag(M_LOCATION,true);
        say(QStringLiteral("位置菜单已经禁用"));
    }
    reloadMenuAll();
}

void Facade::detectPropsMenu()
{
    if(menuFlag.banProps)
    {
        menuFlag.banProps=false;
        menuFlag.setFlag(M_PROPS,false);
        say(QStringLiteral("组件菜单已经启用"));
    }
    else
    {
        menuFlag.banProps=true;
        menuFlag.setFlag(M_PROPS,true);
        say(QStringLiteral("组件菜单已经禁用"));
    }
    reloadMenuAll();
}


void Facade::detectToolMenu()
{
    if(menuFlag.banTool)
    {
        menuFlag.banTool=false;
        menuFlag.setFlag(M_TOOL,false);
        say(QStringLiteral("工具菜单已经启用"));
    }
    else
    {
        menuFlag.banTool=true;
        menuFlag.setFlag(M_TOOL,true);
        say(QStringLiteral("工具菜单已经禁用"));
    }
    reloadMenuAll();
}

void Facade::processCommandAction(QAction *action)
{
    QString cmd=commandMenuItems.value(action->text());
    if(cmd.startsWith("#"))
        return;
    QProcess::startDetached(cmd);
}

void Facade::processToolAction(QAction *action)
{
    QString url=toolMenuItems.value(action->text());
    if(url.startsWith("#"))
        return;
    QDesktopServices::openUrl(QUrl(url));
}

void Facade::processLocationAction(QAction *action)
{
    QString url=locationMenuItems.value(action->text());
    if(url.startsWith("#"))
        return;
    QDesktopServices::openUrl(QUrl(url));
}

void Facade::processFavoriteAction(QAction *action)
{
    QString url=favoriteMenuItems.value(action->text());
    QDesktopServices::openUrl(QUrl(url));
}

void Facade::processPropsAction(QAction *action)
{
    if(!propsList.contains(action))
    {
        QString url=propsMenuItems.value(action->text());
        if(url.startsWith("#"))
            return;
        QDesktopServices::openUrl(QUrl(url));
    }
}

void Facade::processMenuReload(const MenuType &type)
{
    switch (type) {
    case M_LOCATION:
        reloadLocationMenu();
        break;
    case M_TOOL:
        reloadToolMenu();
        break;
    case M_PROPS:
        reloadPropsMenu();
        break;
    case M_COMMAND:
        reloadCommandMenu();
        break;
    }
}

void Facade::showSearchBox()
{
    if(searchBox == nullptr)
    {
        searchBox=new SearchBox(this);
        searchBox->move(d_width /2 - 200,50);
    }
    searchBox->show();
}

void Facade::showNote()
{
    if(note==nullptr)
        note=new Note(this);
    note->move(300,100);
    note->show();
}

void Facade::showNote2()
{
    if(note2==nullptr)
        note2=new Note2(this);
    note2->move(400,200);
    note2->show();
}

void Facade::say(const QString &word, unsigned sec)
{
    curPos=this->pos();
    tor->move(curPos.x() - 50 ,curPos.y() - 100 );
    tor->display(word,sec);
}
