﻿#ifndef G_VER_H
#define G_VER_H

#include "g_def.h"

#define SP_NAME "DesktopMaid"
#define SP_TYPE T_SHELL
#define SP_VER 101

#endif // G_VER_H
