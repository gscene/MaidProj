﻿#ifndef FACADE_H
#define FACADE_H

#include "t_facade.h"
#include "m_expr.h"
#include "figure.h"

#include <QApplication>
#include <QKeyEvent>
#include <QMoveEvent>
#include <QContextMenuEvent>
#include <QCursor>
#include <QThread>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>

namespace Ui {
class Facade;
}

class Facade : public TransFacade
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = 0);
    ~Facade();

    void keyPressEvent(QKeyEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *);
    void contextMenuEvent(QContextMenuEvent *);
    void moveEvent(QMoveEvent *);

    void delay(int sec=4);
    void startUp();
    void say(const QString &, unsigned sec=4);

    void initSpTimer();
    void sessionTick();
    void liveTick();

    void timeSection_Greet();
    void timeSection_Remind();
    void directSay();

    void showNote();
    void showNote2();
    void showSearchBox();

    void createMenuAll();
    void reloadMenuAll();

    void createOptionMenu();

    void createPropsMenu();
    void editPropsMenu();
    void reloadPropsMenu();

    void createCommandMenu();
    void editCommandMenu();
    void reloadCommandMenu();

    void createLocationMenu();
    void editLocationMenu();
    void reloadLocationMenu();

    void createToolMenu();
    void editToolMenu();
    void reloadToolMenu();

    void createFavoriteMenu();
    void editFavoriteMenu();
    void reloadFavoriteMenu();

    void detectLocationMenu();
    void detectToolMenu();
    void detectGroupMenu();
    void detectPropsMenu();
    void detectCommandMenu();
    void detectFavoriteMenu();

private slots:
    void processFavoriteAction(QAction *);
    void processToolAction(QAction *);
    void processLocationAction(QAction *);
    void processCommandAction(QAction *);
    void processPropsAction(QAction *);
    void processMenuReload(const MenuType &type);

private:
    Ui::Facade *ui;

    Sprite sp;
    int hour;
    QString word;
    MenuFlag menuFlag;
    QTime time;

    Torus *tor;
    QTimer *liveTimer;
    SearchBox *searchBox;

    Note *note;
    Note2 *note2;

    QMenu *menu;
    QMenu *m_user;  // Y
    QMenu *m_location;  // Y
    QMenu *m_tool; //Y
    QMenu *m_props; //Y
    QMenu *m_note;
    QMenu *m_option;
    QMenu *m_menuDetect;
    QMenu *m_menuEdit;
    QMenu *m_command;
    QMenu *m_favorite;   //Y


    QAction *showSearchBoxAction;
    QAction *showNoteAction;
    QAction *showNote2Action;

    QAction *editPropsMenuAction;
    QAction *editCommandMenuAction;
    QAction *editLocationMenuAction;
    QAction *editToolMenuAction;
    QAction *editFavoriteMenuAction;

    QAction *detectPropsMenuAction;
    QAction *detectUserMenuAction;
    QAction *detectLocationAction;
    QAction *detectCommandMenuAction;
    QAction *detectToolMenuAction;
    QAction *detectFavoriteMenuAction;

    QAction *moveDefPosAction;
    QAction *hideAction;
    QAction *quitAction;
    QList<QAction *> propsList;

    QHash<QString,QString> commandMenuItems;
    QHash<QString,QString> toolMenuItems;
    QHash<QString,QString> locationMenuItems;
    QHash<QString,QString> favoriteMenuItems;
    QHash<QString,QString> propsMenuItems;
};

#endif // FACADE_H
