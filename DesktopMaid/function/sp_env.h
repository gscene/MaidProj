﻿#ifndef SP_ENV_H
#define SP_ENV_H

#include <QCoreApplication>
#include <QProcess>
#include <QProcessEnvironment>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QApplication>
#include <QRect>
#include <QAction>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QPixmap>

#include <QUrl>
#include <QCloseEvent>
#include <QString>
#include <QStringList>
#include <QList>
#include <QHash>
#include <QMap>

#include <QSettings>
#include <QUuid>
#include <QTime>
#include <QDate>
#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QDir>

#include <QHostInfo>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QAbstractSocket>
#include <QTimer>

#include <QTextStream>
#include <QDataStream>
#include <QObject>
#include <QUdpSocket>

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QModelIndex>

#include "m_fhs.h"

struct Env
{
    static QString hostName;
};

struct Path
{
    static QString home;
    static QString local;

    static QString music;
    static QString picture;
    static QString video;
    static QString document;
    static QString download;
    static QString desktop;
    static QString apps;
};

#endif // SP_ENV_H
