﻿#ifndef M_FHS
#define M_FHS

#define APP_ROOT "/DesktopMaid"
#define FAV_FILE "/DesktopMaid/favorite.json"

#define e_stat "/DesktopMaid/stat.dat"
#define sp_stat "/DesktopMaid/stat.txt"

#define MENU_FILE "../etc/menu.ini"

#define MENU_TOOL "../etc/menu/tool.json"
#define MENU_GROUP "../etc/menu/group.json"
#define MENU_LOCATION "../etc/menu/location.json"
#define MENU_CMD "../etc/menu/command.json"
#define MENU_PROPS "../etc/menu/props.json"

#define README "../share/readme.html"

#define e_reg "HKEY_CURRENT_USER\\Software\\elfproj\\DesktopMaid"

#define e_note "/DesktopMaid/note.txt"
#define e_note21 "/DesktopMaid/note21.txt"
#define e_note22 "/DesktopMaid/note22.txt"

#define e_std "../lib/std/std.rpk"
#define e_std_f "../lib/std/std_f.rpk"
#define e_spec "../lib/std/spec.rpk"
#define e_spring "../lib/std/spring.rpk"
#define e_spring_f "../lib/std/spring_f.rpk"
#define e_summer "../lib/std/summer.rpk"

#endif // M_FHS

