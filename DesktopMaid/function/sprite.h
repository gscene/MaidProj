﻿#ifndef SPRITE_H
#define SPRITE_H

#include "sp_heart.h"
#include "master.h"

enum DressType
{
    Maid_Suit,      //女仆装,std
    Maid_Suit_F,    //flank,std01
    Spring_Suit,    //春装,std10
    SPring_Suit_F, //flank,std11
    Summer_Suit,    //夏装,std20
    DT_Bathrobe    //浴衣,std30
};

class Sprite : public SpriteHeart
{
public:
    void wakeUp();
    bool isInstalled();

    QString getUid() const;
    void putUid(const QString &uid);

    void sleep();
    void displayState();

private:
    Master _mas;
};

#endif // SPRITE_H
