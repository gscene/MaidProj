﻿#ifndef MASTER_H
#define MASTER_H

#include "sp_env.h"

struct Master
{
    QString name;       //本名,eg:mud
    QString alias;          //别名,eg:fox
   // QString nickname;       //绰号,eg:蚂蚁
    QString honor;          //称谓，非正式称谓，现在用于称呼

    void putName(const QString &name);
    QString getName();

    void putAlias(const QString &alias);
    QString getAlias();

 //   void putNickname(const QString &nickname);
    void putHonor(const QString &honor);
    QString getHonor();

    void setInfo();
    void putInfo();
    void getInfo();
};

#endif // MASTER_H
