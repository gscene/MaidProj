﻿#include "sprite.h"

bool Sprite::isInstalled()
{
    QString uid=getUid();
    if(uid.isEmpty())
        return false;
    else
        return true;
}

void Sprite::wakeUp()
{
    if(!isInstalled())
    {
        QString uid=QUuid::createUuid().toString();
        putUid(uid);
        initState();
    }
    else
        loadState();
}

void Sprite::putUid(const QString &uid)
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    reg.setValue("spUid",uid);
}

QString Sprite::getUid() const
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    return reg.value("spUid").toString();
}

void Sprite::sleep()
{
    saveState();
    displayState();
}

void Sprite::displayState()
{
    QFile file(Path::document + sp_stat);
    file.open(QFile::WriteOnly | QFile::Text);
    QTextStream text(&file);
    text << "SP: " << sessionPoint << "\n"
            << "LP: " << livePoint;
    file.close();
}
