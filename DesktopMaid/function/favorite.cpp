﻿#include "favorite.h"

bool Favorite::load(const QString &file)
{
    QFile config(file);
    if(config.exists())
    {
        YAML::Node doc=YAML::LoadFile(file.toStdString());
        if(doc.IsSequence())
        {
            for(int i=0;i<doc.size();i++)
            {
                screw(doc[i]);
            }
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

void Favorite::screw(const YAML::Node &node)
{
    QString name,url;
    name=QString::fromStdString(node["name"].as<std::string>());
    url=QString::fromStdString(node["url"].as<std::string>());
    items.insert(name,url);
}

void Favorite::open(QString &name)
{
    QDesktopServices::openUrl(QUrl(items.value(name)));
}
