﻿#ifndef FAVORITE_H
#define FAVORITE_H

#include "sp_env.h"

#define PRE_HTTP "http"
#define URL_HTTP "http://"

struct Favorite
{
    QMap<QString,QString> items;

    bool load(const QString &file=FAV_FILE);
    void screw(const YAML::Node &node);

    void open(QString &);
};

#endif // FAVORITE_H
