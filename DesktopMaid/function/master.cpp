#include "Master.h"

void Master::putName(const QString &name)
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    reg.setValue("Name",name);
}

QString Master::getName()
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    name=reg.value("Name").toString();
    if(name.isEmpty())
        name=QStringLiteral("未知用户");
    return name;
}

void Master::putAlias(const QString &alias)
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    reg.setValue("Alias",alias);
}

QString Master::getAlias()
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    name=reg.value("Alias").toString();
    if(name.isEmpty())
        name=QStringLiteral("未知");
    return name;
}

void Master::putHonor(const QString &honor)
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    reg.setValue("Honor",honor);
}

QString Master::getHonor()
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    name=reg.value("Honor").toString();
    if(name.isEmpty())
        name=QStringLiteral("主人");
    return name;
}

void Master::setInfo()
{
    if(Env::hostName.startsWith("mud-"))
    {
        name="mud";
        alias="fox";
    }
    else{
        name="";
        alias="";
    }
    honor=QStringLiteral("主人");
    putInfo();
}

void Master::putInfo()
{
    QSettings reg(e_reg,QSettings::NativeFormat);
    reg.setValue("Name",name);
    reg.setValue("Alias",alias);
    reg.setValue("Honor",honor);
}
