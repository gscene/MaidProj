﻿#ifndef SPRITEHEART_H
#define SPRITEHEART_H

#include "m_heart.h"
#include "sp_env.h"

class SpriteHeart
{
public:
    Famility getFamility() const;
    Favility getFavility() const;

    void initState();
    void loadState();
    void saveState();

    void sessionUpdate();
    void liveUpdate();

protected:
    Personility pity;

    quint32 sessionPoint;      //会话计数，超过10秒为1次,sp_session
    quint32 livePoint;             //活跃计数，每15分钟加1点,sp_live

    bool sessionFlag;
    bool liveFlag;
};

#endif // SPRITEHEART_H
