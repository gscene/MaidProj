﻿#include "sp_env.h"

QString Env::hostName=QHostInfo::localHostName();

QString Path::home=QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
QString Path::local=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
QString Path::music=QStandardPaths::writableLocation(QStandardPaths::MusicLocation);
QString Path::picture=QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
QString Path::video=QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
QString Path::document=QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
QString Path::download=QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
QString Path::desktop=QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
QString Path::apps=QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
