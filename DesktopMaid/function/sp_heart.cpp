﻿#include "sp_heart.h"

void SpriteHeart::initState()
{
    sessionPoint=0;
    livePoint=0;
    saveState();
}

void SpriteHeart::saveState()
{
    QFile file(Path::document + e_stat);
    if(file.open(QFile::ReadWrite))
    {
        QDataStream dat(&file);
        dat << sessionPoint;
        dat << livePoint;
        file.close();
    }
}

void SpriteHeart::loadState()
{
    QFile file(Path::document + e_stat);
    if(file.open(QFile::ReadOnly))
    {
        QDataStream dat(&file);
        dat >> sessionPoint >> livePoint;
        file.close();
    }
}

void SpriteHeart::sessionUpdate()
{
    sessionPoint +=1;
}

void SpriteHeart::liveUpdate()
{
    livePoint +=1;
}

Famility SpriteHeart::getFamility() const
{
    if(sessionPoint >= 999)
        return FAM_FULL;
    else if(sessionPoint >=50)
        return FAM_H;
    else if(sessionPoint >=10 )
        return FAM_M;
    else if(sessionPoint >=3)
        return FAM_L;
    else
        return FAM_NULL;
}

Favility SpriteHeart::getFavility() const
{
    if(livePoint >= 99 )
        return FAV_FULL;
    else if(livePoint >= 50)
        return FAV_H;
    else if(livePoint >= 20)
        return FAV_M;
    else if(livePoint >= 3)
        return FAV_L;
    else
        return FAV_NULL;
}
