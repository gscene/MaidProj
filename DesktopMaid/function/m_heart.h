﻿#ifndef M_HEART
#define M_HEART

//熟悉度
enum Famility
{
    FAM_NULL=0,          //陌生，对于陌生人的标记,0
    FAM_L,              //有点熟悉,3
    FAM_M,              //比较熟悉,10
    FAM_H,              //很熟悉,30
    FAM_FULL        //熟悉度已满，99
};

//好感度
enum Favility
{
    // FAV_FULL,     //好感度负无穷，拒绝启动
    // FAV_NH,
    // FAV_NM,
    // FAV_NL,         //Negative LOW,好感度为负值，

    FAV_NULL=0,          //默认好感度,0
    FAV_L,         //Positive LOW，好感度低,3
    FAV_M,         //MED,20
    FAV_H,          //HIGH,70
    FAV_FULL        //好感度已满，停止计数,99
};

//预置性格
enum Personility
{
    Gently,     //温柔
    Lovely,     //乖巧，懂事
    Missy,      //傲娇
    Noisy ,    //聒噪，腹黑
    Smarty      //智能机器人
};

//当前心情
enum Mood
{
    MOD_CALM,       //平静
    MOD_EXCIT,      //兴奋
    MOD_RELAX,      //轻松
    MOD_NERV,       //紧张
    MOD_CHEER,     //愉快
    MOD_MIX,        //复杂
};

//当前感受
enum Feeling
{
    FEL_BAD,        //差
    FEL_WEL,        //好
    FEL_WOR,        //很差
};


#endif // M_HEART

