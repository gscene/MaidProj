﻿#include "facade.h"
#include <QResource>
#include <QtSingleApplication>
#include "g_function.h"

int main(int argc, char *argv[])
{
    QtSingleApplication  a(SP_NAME,argc, argv);

    if(argc == 2)
    {
        if(strcmp(argv[1],"-v") == 0)
        {
            sp_createRunFile(a.applicationName());
            return EXIT_SUCCESS;
        }
    }

    if(a.isRunning())
    {
        QMessageBox::critical(0,QStringLiteral("已经在运行"),QStringLiteral("已经有一个实例在运行中！"),
                              QMessageBox::Ok);
        return EXIT_SUCCESS;
    }

    a.setQuitOnLastWindowClosed(false);

    QResource::registerResource(e_std);

    Facade w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}