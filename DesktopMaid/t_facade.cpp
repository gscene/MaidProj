﻿#include "t_facade.h"

TransFacade::TransFacade(QWidget *parent) :
    QWidget(parent)
{
    setAttribute(Qt::WA_TranslucentBackground);

    setWindowFlags(
                Qt::FramelessWindowHint |
                Qt::Tool |
                Qt::WindowStaysOnTopHint
                );

    createTray();
    d_width=qApp->desktop()->availableGeometry().width();
    d_height=qApp->desktop()->availableGeometry().height();
    setDefPos();
}

void TransFacade::mousePressEvent(QMouseEvent *event)
{
    winPos=this ->pos();
    mosPos= event ->globalPos();
    dPos=mosPos - winPos;
}

void TransFacade::mouseMoveEvent(QMouseEvent *event)
{
    this->move(event ->globalPos() - dPos );
}

void TransFacade::detectVisible()
{
    if(this->isVisible())
        this->hide();
    else
        this->show();
}

void TransFacade::createTray()
{
    tray_Menu=new QMenu(this);

    tray_exitAction=tray_Menu->addAction(QStringLiteral("退出"));
    connect(tray_exitAction,&QAction::triggered,this,&QApplication::quit);

    tray=new QSystemTrayIcon(QIcon(":/tray.ico"),this);
    tray->setContextMenu(tray_Menu);
  //  tray->setToolTip(APP_INFO);

    connect(tray,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this,SLOT(trayActivated(QSystemTrayIcon::ActivationReason))
            );
    tray->show();
}

void TransFacade::trayActivated(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::DoubleClick)
        detectVisible();
}

void TransFacade::setDefPos()
{
    defPos.setX(d_width - 500);
    defPos.setY( DEF_HEI + 180);
    this->move(defPos);
}

void TransFacade::moveDefPos()
{
    this->move(defPos);
}

