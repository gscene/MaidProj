﻿#ifndef NOTE_H
#define NOTE_H

#include <QDialog>
#include "sp_env.h"

namespace Ui {
class Note;
}

class Note : public QDialog
{
    Q_OBJECT

public:
    explicit Note(QWidget *parent = 0);
    ~Note();

    void closeEvent(QCloseEvent *);
    void save();
    void firstLoad();

private slots:
    void on_in_date_clicked();
    void on_in_time_clicked();

private:
    Ui::Note *ui;
};

#endif // NOTE_H
