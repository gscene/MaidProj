﻿#include "favoriteeditor.h"
#include "ui_favoriteeditor.h"

FavoriteEditor::FavoriteEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FavoriteEditor)
{
    ui->setupUi(this);

    model=nullptr;
    saveFlag=0;
    helper=new HTSModel(this);
}

FavoriteEditor::~FavoriteEditor()
{
    delete ui;
}

bool FavoriteEditor::detect(const QString &fileName)
{
    _file=fileName;
    _title=QStringLiteral("收藏夹");

    QFile file(_file);
    if(!file.exists())
    {
        if(createFavorite())
            return showEditor();
        else
            return false;
    }
    else
        return showEditor();
}

bool FavoriteEditor::createFavorite()
{
    QJsonObject obj;
    obj.insert(QString(HT_HEAD),DEF_FAV_NAME);
    obj.insert(QString(HT_TAIL),DEF_FAV_URL);

    QJsonArray array;
    array.append(obj);
    helper->setTitle(_title);
    helper->setJsonArray(array);
    saveFlag=-1;
    return helper->save(_file);
}

bool FavoriteEditor::showEditor()
{
    if(helper->load(_file))
    {
        _title=helper->getTitle();
        headList=helper->getHeadList();
        model=helper->getModel();
        model->setHeaderData(0,Qt::Horizontal,QStringLiteral("名称"));
        model->setHeaderData(1,Qt::Horizontal,QStringLiteral("网址"));
        ui->tableView->setModel(model);
        return true;
    }
    else
        return false;
}

void FavoriteEditor::on_btn_add_clicked()
{
    name=ui->name->text();
    url=ui->url->text();

    ui->name->clear();
    ui->url->clear();
    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();

    if(name.isEmpty() || url.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("名称、路径不能为空！"),
                             QMessageBox::Ok);
        return;
    }
    if(headList.contains(name))
    {
        QMessageBox::warning(this,QStringLiteral("名称不能重复"),name + QStringLiteral("已经存在！"),
                             QMessageBox::Ok);
        return;
    }

    if(!url.startsWith(PRE_HTTP) && !url.startsWith(PRE_HTTPS))
        url=PRE_HTTP + url;

    saveFlag=-1;
    headList.append(name);
    QList<QStandardItem *> item;
    item.append(new QStandardItem(name));
    item.append(new QStandardItem(url));
    model->appendRow(item);
    helper->append(name,url);
}

void FavoriteEditor::on_btn_del_clicked()
{
    if(model->rowCount() >1)
    {
        int row=ui->tableView->currentIndex().row();
        headList.removeAt(row);
        model->removeRow(row);
        helper->remove(row);
        saveFlag=-1;
    }
    else
        QMessageBox::warning(this,QStringLiteral("至少保留一项"),QStringLiteral("至少保留一项！"),
                             QMessageBox::Ok);

    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();
}

void FavoriteEditor::on_btn_save_clicked()
{
    switch (saveFlag) {
    case 0:
        QMessageBox::warning(this,QStringLiteral("菜单未修改"),QStringLiteral("菜单未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    case -1:
        if(helper->save(_file))
        {
            //       ui->lbl_status->setText(QStringLiteral("保存成功！"));
            saveFlag=1;
            emit reload();
            accept();
        }
        else
            ui->lbl_status->setText(QStringLiteral("保存失败！"));
        break;
    case 1:
        QMessageBox::warning(this,QStringLiteral("新菜单未修改"),QStringLiteral("新菜单未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    }
}
