﻿#ifndef NOTE2_H
#define NOTE2_H

#include <QDialog>
#include "sp_env.h"

namespace Ui {
class Note2;
}

class Note2 : public QDialog
{
    Q_OBJECT

public:
    explicit Note2(QWidget *parent = 0);
    ~Note2();

    void closeEvent(QCloseEvent *);
    void save();
    void firstLoad();

private:
    Ui::Note2 *ui;
};

#endif // NOTE2_H
