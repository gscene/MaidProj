﻿#include "menueditor.h"
#include "ui_menueditor.h"

MenuEditor::MenuEditor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MenuEditor)
{
    ui->setupUi(this);

    model=nullptr;
    saveFlag=0;
    helper=new HTSModel(this);
}

MenuEditor::~MenuEditor()
{
    delete ui;
}

bool MenuEditor::detect(const MenuType &type)
{
    _type=type;
    switch (_type) {
    case M_LOCATION:
        _file=MENU_LOCATION;
        _title=QStringLiteral("位置");
        break;
    case M_TOOL:
        _file=MENU_TOOL;
        _title=QStringLiteral("工具");
        break;
    case M_GROUP:
        _file=MENU_GROUP;
        _title=QStringLiteral("组别");
        break;
    case M_PROPS:
        _file=MENU_PROPS;
        _title=QStringLiteral("组件");
        break;
    case M_COMMAND:
        _file=MENU_CMD;
        _title=QStringLiteral("命令");
        break;
    }

    QFile file(_file);
    if(!file.exists())
    {
        if(createProps())
            return showEditor();
        else
            return false;
    }
    else
        return showEditor();
}

bool MenuEditor::createProps()
{
    QJsonObject obj;
    obj.insert(QString(HT_HEAD),DEF_MENU_NAME);
    obj.insert(QString(HT_TAIL),DEF_MENU_URL);

    QJsonArray array;
    array.append(obj);
    helper->setTitle(_title);
    helper->setJsonArray(array);
    saveFlag=-1;
    return helper->save(_file);
}

bool MenuEditor::showEditor()
{
    if(helper->load(_file))
    {
        _title=helper->getTitle();
        setWindowTitle(_title + QStringLiteral("菜单编辑器"));
        headList=helper->getHeadList();
        model=helper->getModel();
        model->setHeaderData(0,Qt::Horizontal,QStringLiteral("名称"));
        model->setHeaderData(1,Qt::Horizontal,QStringLiteral("路径"));
        ui->tableView->setModel(model);
        return true;
    }
    else
        return false;
}

void MenuEditor::on_btn_open_clicked()
{
    QUrl url=QFileDialog::getOpenFileUrl(this,QStringLiteral("选择文件"));
    ui->path->setText(url.toString());
}

void MenuEditor::on_btn_add_clicked()
{
    name=ui->name->text();
    path=ui->path->text();

    ui->name->clear();
    ui->path->clear();
    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();

    if(name.isEmpty() || path.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("名称、路径不能为空！"),
                             QMessageBox::Ok);
        return;
    }
    if(headList.contains(name))
    {
        QMessageBox::warning(this,QStringLiteral("名称不能重复"),name + QStringLiteral("已经存在！"),
                             QMessageBox::Ok);
        return;
    }

    saveFlag=-1;
    headList.append(name);

    QList<QStandardItem *> item;
    item.append(new QStandardItem(name));
    item.append(new QStandardItem(path));
    model->appendRow(item);
    helper->append(name,path);
}

void MenuEditor::on_btn_del_clicked()
{
    if(model->rowCount() > 1)
    {
        int row=ui->tableView->currentIndex().row();
        headList.removeAt(row);
        model->removeRow(row);
        helper->remove(row);
        saveFlag=-1;
    }
    else
        QMessageBox::warning(this,QStringLiteral("至少保留一项"),QStringLiteral("至少保留一项！"),
                             QMessageBox::Ok);

    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();
}

void MenuEditor::on_btn_save_clicked()
{
    switch (saveFlag) {
    case 0:
        QMessageBox::warning(this,QStringLiteral("菜单未修改"),QStringLiteral("菜单未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    case -1:
        if(helper->save(_file))
        {
            //         ui->lbl_status->setText(QStringLiteral("保存成功！"));
            saveFlag=1;
            emit reload(_type);
            accept();
        }
        else
            ui->lbl_status->setText(QStringLiteral("保存失败！"));
        break;
    case 1:
        QMessageBox::warning(this,QStringLiteral("新菜单未修改"),QStringLiteral("新菜单未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    }
}
