﻿#ifndef MENUEDITOR_H
#define MENUEDITOR_H

#include <QDialog>
#include "sp_env.h"
#include "htsmodel.h"

#define DEF_MENU_NAME QStringLiteral("示例标题")
#define DEF_MENU_URL QStringLiteral("#示例路径")

namespace Ui {
class MenuEditor;
}

enum MenuType
{
    M_LOCATION,
    M_TOOL,
    M_GROUP,
    M_PROPS,
    M_FAVORITE,
    M_COMMAND
};

class MenuEditor : public QDialog
{
    Q_OBJECT

public:
    explicit MenuEditor(QWidget *parent = 0);
    ~MenuEditor();

    bool detect(const MenuType &type);
    bool createProps();
    bool showEditor();

signals:
    void reload(const MenuType &type);

private slots:
    void on_btn_open_clicked();

    void on_btn_add_clicked();

    void on_btn_del_clicked();

    void on_btn_save_clicked();

private:
    Ui::MenuEditor *ui;

    HTSModel *helper;
    QStringList headList;
    QString name,path;
    QString _title;
    QString _file;
    int saveFlag;       // 0 未修改，-1 未保存，1已保存

    MenuType _type;
    QStandardItemModel *model;
};

#endif // MENUEDITOR_H
