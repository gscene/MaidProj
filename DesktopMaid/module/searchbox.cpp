﻿#include "searchbox.h"
#include "ui_searchbox.h"

SearchBox::SearchBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SearchBox)
{
    ui->setupUi(this);
}

SearchBox::~SearchBox()
{
    delete ui;
}

void SearchBox::on_btn_search_clicked()
{
    inputStr=ui->in_search->text();
    if(inputStr.isEmpty())
        return;

    searchStr= SEARCH + inputStr;
    QDesktopServices::openUrl(QUrl(searchStr));
}
