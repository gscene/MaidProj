﻿#include "torus.h"
#include "ui_torus.h"

Torus::Torus(QWidget *parent) :
    TransDialog(parent),
    ui(new Ui::Torus)
{
    ui->setupUi(this);
    //   setAttribute(Qt::WA_DeleteOnClose);

    TORUS_PIK;

    /*
            timerFade=new QTimer(this);
            connect(timerFade,&QTimer::timeout,this,&Torus::fade);
    */
}

Torus::~Torus()
{
    delete ui;
}

/*
void Torus::fade()
{
    static double tran=1.0;
    tran-=0.1;

    if(tran<=0.0)
    {
        timerFade->stop();
        tran=1.0;
        this->hide();
    }
    else
        setWindowOpacity(tran);
}

void Torus::timeout()
{
    timerFade->start(100);
}
*/

void Torus::display(const QString &words, unsigned sec)
{
    if(!this->isVisible())
        this->show();
    ui->body->setText(words);
    QTimer::singleShot(1000 * sec,this,&Torus::hide);
}

void Torus::setStyle(int num)
{
    if(num == sk_blu)
        TORUS_BLU;
    else if(num == sk_pik)
        TORUS_PIK;
    else if(num == sk_gre)
        TORUS_GRE;
    else
        TORUS_PIK;
}
