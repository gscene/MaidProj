﻿#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include <QDialog>
#include <QString>
#include <QDesktopServices>
#include <QUrl>

#define SEARCH "http://www.baidu.com/s?wd="

namespace Ui {
class SearchBox;
}

class SearchBox : public QDialog
{
    Q_OBJECT

public:
    explicit SearchBox(QWidget *parent = 0);
    ~SearchBox();

private slots:
    void on_btn_search_clicked();

private:
    Ui::SearchBox *ui;
    QString inputStr;
    QString searchStr;
};

#endif // SEARCHBOX_H
