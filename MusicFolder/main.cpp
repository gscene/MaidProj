﻿#include "musicfolder.h"
#include <QtSingleApplication>

int main(int argc, char *argv[])
{
    QtSingleApplication a(argc, argv);
    if(a.isRunning())
    {
        QMessageBox::critical(0,"ERROR",QStringLiteral("已经有一个实例在运行！"));
        return EXIT_SUCCESS;
    }

    MusicFolder w;
    if(!w.initPlayer())
    {
        QMessageBox::critical(0,"ERROR",QStringLiteral("播放器初始化失败！"));
        return EXIT_SUCCESS;
    }
    w.initView();
    w.show();

    return a.exec();
}
