﻿#ifndef PLAYER_H
#define PLAYER_H

#include <QString>
#include <QByteArray>
#include <string>
#include <fmod/fmod.hpp>
#include <fmod/fmod_errors.h>



class Player
{
public:
    enum PlayerState
    {
        Stop,
        Play,
        Pause
    };

    bool init();
    void close();
    bool play(const QString &fileName);
    void stop();
    void pause();
    void resume();
    void detectState();
    bool isPlaying();

private:
    PlayerState state;
    FMOD::System *m_pSystem;
    FMOD::Sound *m_pSound;
    FMOD::Channel *m_pChannel;
    FMOD_RESULT result1,result2;
};

#endif // PLAYER_H
