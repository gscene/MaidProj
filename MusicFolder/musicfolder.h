﻿#ifndef MUSICFOLDER_H
#define MUSICFOLDER_H

#include <QWidget>
#include <QMessageBox>
#include <QStandardPaths>
#include <QFileSystemModel>
#include <QFileInfo>
#include <QListView>
#include <QTreeView>
#include <player.h>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QKeyEvent>

#define SONG_LIST "../var/log/songs.txt"

namespace Ui {
class MusicFolder;
}

class MusicFolder : public QWidget
{
    Q_OBJECT

public:
    explicit MusicFolder(QWidget *parent = 0);
    ~MusicFolder();

    void keyPressEvent(QKeyEvent *event);

    bool initPlayer();
    void initView();

    bool play(const QString &fileName);
    void log(const QString &fileName);

private slots:
    void processMusicIndex(const QModelIndex &index);

private:
    Ui::MusicFolder *ui;
    QFileSystemModel *model;
    Player *player;
};

#endif // MUSICFOLDER_H
