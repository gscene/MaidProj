#-------------------------------------------------
#
# Project created by QtCreator 2015-12-26T10:50:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = MusicFolder
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += D:\usr\include
LIBS += -LD:\usr\lib -lfmodex64

SOURCES += main.cpp\
        musicfolder.cpp \
    player.cpp

HEADERS  += musicfolder.h \
    player.h

FORMS    += musicfolder.ui

RESOURCES += \
    res.qrc
