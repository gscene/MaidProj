﻿#ifndef FAVORITE_H
#define FAVORITE_H

#include "sp_env.h"

#include "vdb.h"

#define PRE_HTTP "http"
#define HTTP "http://"

struct Favorite
{
    QString url_1,url_2,url_3,url_4,url_5;
    QString url_6,url_7,url_8,url_9,url_0;

    QString u1_name,u2_name,u3_name,u4_name,u5_name;
    QString u6_name,u7_name,u8_name,u9_name,u0_name;

    void load();
    void save();

    void open(int);
};

#endif // FAVORITE_H
