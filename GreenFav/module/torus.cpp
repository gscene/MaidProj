﻿#include "torus.h"
#include "ui_torus.h"

Torus::Torus(QWidget *parent) :
    TransDialog(parent),
    ui(new Ui::Torus)
{
    ui->setupUi(this);

    TORUS_GRE;
}

Torus::~Torus()
{
    delete ui;
}

void Torus::display(const QString &words, unsigned sec)
{
    if(!this->isVisible())
        this->show();
    ui->body->setText(words);
    QTimer::singleShot(1000 * sec,this,&Torus::hide);
}
