﻿#ifndef NOTE3_H
#define NOTE3_H

#include <QDialog>
#include <QTextStream>
#include "sp_env.h"

namespace Ui {
class Note3;
}

class Note3 : public QDialog
{
    Q_OBJECT

public:
    explicit Note3(QWidget *parent = 0);
    ~Note3();

    void closeEvent(QCloseEvent *);
    void save();
    void firstLoad();

private:
    Ui::Note3 *ui;
};

#endif // NOTE3_H
