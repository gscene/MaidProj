﻿#ifndef FAVORITEEDITOR_H
#define FAVORITEEDITOR_H

#include <QDialog>
#include <QMessageBox>
#include "htsmodel.h"

#define PRE_HTTP "http://"
#define PRE_HTTPS "https://"

#define DEF_NAME QStringLiteral("百度")
#define DEF_URL "https://www.baidu.com/"

namespace Ui {
class FavoriteEditor;
}

class FavoriteEditor : public QDialog
{
    Q_OBJECT

public:
    explicit FavoriteEditor(QWidget *parent = 0);
    ~FavoriteEditor();

    bool detect(const QString &fileName);
    bool createFavorite();
    bool showFavorite();

signals:
    void refresh();

private slots:
    void on_btn_save_clicked();

    void on_btn_add_clicked();

    void on_btn_del_clicked();

private:
    Ui::FavoriteEditor *ui;

    HTSModel *helper;
    QStringList headList;
    QString name,url;
    QString _title;
    QString _file;
    int saveFlag;       // 0 未修改，-1 未保存，1已保存
    QStandardItemModel *model;
};

#endif // FAVORITEEDITOR_H
