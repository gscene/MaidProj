﻿#include "note3.h"
#include "ui_note3.h"

Note3::Note3(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Note3)
{
    ui->setupUi(this);

    firstLoad();
}

Note3::~Note3()
{
    delete ui;
}

void Note3::firstLoad()
{
    QFile file1(Env::documentLocation + e_note31);

    if(file1.exists())
    {
        QTextStream text1(&file1);
        file1.open(QFile::ReadWrite | QFile::Text);
        QString content1=text1.readAll();
        ui->textEdit_1->setText(content1);
        file1.close();
    }

    QFile file2(Env::documentLocation  + e_note32);

    if(file2.exists())
    {
        QTextStream text2(&file2);
        file2.open(QFile::ReadWrite | QFile::Text);
        QString content2=text2.readAll();
        ui->textEdit_2->setText(content2);
        file2.close();
    }

    QFile file3(Env::documentLocation + e_note33);

    if(file3.exists())
    {
        QTextStream text3(&file3);
        file3.open(QFile::ReadWrite | QFile::Text);
        QString content3=text3.readAll();
        ui->textEdit_3->setText(content3);
        file3.close();
    }
}

void Note3::closeEvent(QCloseEvent *)
{
    save();
    accept();
}

void Note3::save()
{
    QFile file1(Env::documentLocation + e_note31);
    file1.open(QFile::WriteOnly | QFile::Text);
    QTextStream text1(&file1);
    text1 << ui->textEdit_1 ->toPlainText();
    file1.close();

    QFile file2(Env::documentLocation + e_note32);
    file2.open(QFile::WriteOnly | QFile::Text);
    QTextStream text2(&file2);
    text2 << ui->textEdit_2 ->toPlainText();
    file2.close();

    QFile file3(Env::documentLocation + e_note33);
    file3.open(QFile::WriteOnly | QFile::Text);
    QTextStream text3(&file3);
    text3 << ui->textEdit_3 ->toPlainText();
    file3.close();
}
