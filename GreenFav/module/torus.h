﻿#ifndef TORUS_H
#define TORUS_H

#include "t_dialog.h"
#include <QString>
#include <QTimer>

#define TORUS_GRE ui->body->setStyleSheet("border-image: url(:/res/bg_green.png)")

namespace Ui {
class Torus;
}

class Torus : public TransDialog
{
    Q_OBJECT

public:
    explicit Torus(QWidget *parent = 0);
    ~Torus();

    void display(const QString &,unsigned sec=4);

private:
    Ui::Torus *ui;
};

#endif // TORUS_H
