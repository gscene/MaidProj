﻿#ifndef FACADE_H
#define FACADE_H

#include "t_facade.h"

#include <QKeyEvent>
#include <QMoveEvent>
#include <QContextMenuEvent>
#include <QCursor>

#include "codeparser.h"
#include "htshandler.h"
#include "favoriteeditor.h"
#include "torus.h"

//exp
#define exp_akd ui->body->setStyleSheet("image:url(:/std/exp_akd.png)")
#define exp_sml ui->body->setStyleSheet("image:url(:/std/exp_sml.png)")
#define exp_nml ui->body->setStyleSheet("image:url(:/std/exp_nml.png)")

namespace Ui {
class Facade;
}

class Facade : public TransFacade
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = 0);
    ~Facade();

    void mouseDoubleClickEvent(QMouseEvent *);
    void contextMenuEvent(QContextMenuEvent *);
    void moveEvent(QMoveEvent *);

    void delay(int sec=4);
    void say(const QString &, unsigned sec=4);

    void initProps();

    void createMenu();
    void showMenu();

    void createFavoriteMenu();
    void reloadFavoriteMenu();
    void editFavoriteMenu();

private slots:
    void processFavoriteAction(QAction *);

private:
    Ui::Facade *ui;
    QTime time;
    Torus *tor;
    QClipboard *clipboard;

    QMenu *menu;
    QMenu *m_option;
    QMenu *m_favorite;

    QAction *editFavoriteAction;
    QAction *moveDefPosAction;
    QAction *hideAction;

    QStringList favoriteHead;
    QHash<QString,QString> favoriteMenuItems;

    QAction *quitAction;
};

#endif // FACADE_H
