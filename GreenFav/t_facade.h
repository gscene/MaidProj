﻿#ifndef T_FACADE_H
#define T_FACADE_H

#include <QApplication>
#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QDesktopWidget>
#include <QRect>
#include <QAction>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QIcon>

#define APP_VER QStringLiteral("绿坝娘的收藏夹 ver. 0.2.0")

#define DEF_WID 200
#define DEF_HEI 400

class TransFacade : public QWidget
{
    Q_OBJECT

public:
    explicit TransFacade(QWidget *parent = 0);

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void detectVisible();
    void createTray();
    void moveDefPos();
    void setDefPos();

protected slots:
    void trayActivated(QSystemTrayIcon::ActivationReason);

private:
    QSystemTrayIcon *tray;
    QMenu *tray_Menu;
    QAction *tray_exitAction;

protected:
    QPoint dPos;
    QPoint defPos;
    QPoint curPos;

    int d_width;
    int d_height;
};

#endif // T_FACADE_H
