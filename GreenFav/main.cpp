﻿#include "facade.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    Facade w;
    w.show();

    return a.exec();
}
