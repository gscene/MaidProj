﻿#ifndef VDB_H
#define VDB_H

#include <QString>
#include <string>
#include "vedis.h"

#define VDB_UKN "UNKNOWN"
#define VDB_ERR "ERROR"

class VDB
{
public:
    VDB();
    ~VDB();

    bool open(const char *);
    bool put(QString , QString );
    QString get(QString );
    bool del(QString);
    void close();

private:
    vedis *pStore;
    vedis_value *pResult;
    int rc;
    std::string cmdStr;
};

#endif // VDB_H
