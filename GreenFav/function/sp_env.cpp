﻿#include "sp_env.h"

QString Env::home=QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
QString Env::local=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
QString Env::hostName=QHostInfo::localHostName();
QString Env::doc=QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
