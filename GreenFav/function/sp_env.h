﻿#ifndef SP_ENV_H
#define SP_ENV_H

#include <QCoreApplication>
#include <QDesktopServices>
#include <QUrl>
#include <QRegExp>
#include <QStandardPaths>
#include <QProcessEnvironment>
#include <QCloseEvent>
#include <QSettings>
#include <QString>
#include <QTime>
#include <QFile>
#include <QList>
#include <QTimer>
#include <QMessageBox>
#include<QModelIndex>
#include <QClipboard>

#define FAV_FILE "/DesktopMaid/favorite.dat"

static QString PATH_DOC=QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);


#endif // SP_ENV_H
