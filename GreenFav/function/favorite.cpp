﻿#include "favorite.h"

void Favorite::load()
{
    QString file=Env::doc + VDB_FILE;
    std::string fileName=file.toStdString();

    VDB vdb;
    if(vdb.open(fileName.c_str()))
    {
        url_1=vdb.get("url_1");
        if(url_1 == VDB_UKN)
            url_1.clear();

        url_2=vdb.get("url_2");
        if(url_2 == VDB_UKN)
            url_2.clear();

        url_3=vdb.get("url_3");
        if(url_3 == VDB_UKN)
            url_3.clear();

        url_4=vdb.get("url_4");
        if(url_4 == VDB_UKN)
            url_4.clear();

        url_5=vdb.get("url_5");
        if(url_5 == VDB_UKN)
            url_5.clear();

        url_6=vdb.get("url_6");
        if(url_6 == VDB_UKN)
            url_6.clear();

        url_7=vdb.get("url_7");
        if(url_7 == VDB_UKN)
            url_7.clear();

        url_8=vdb.get("url_8");
        if(url_8 == VDB_UKN)
            url_8.clear();

        url_9=vdb.get("url_9");
        if(url_9 == VDB_UKN)
            url_9.clear();

        url_0=vdb.get("url_0");
        if(url_0 == VDB_UKN)
            url_0.clear();

        /*
         *
        item_1.setUrl(url_1);
        item_2.setUrl(url_2);
        item_3.setUrl(url_3);
        item_4.setUrl(url_4);
        item_5.setUrl(url_5);
        item_6.setUrl(url_6);
        item_7.setUrl(url_7);
        item_8.setUrl(url_8);
        item_9.setUrl(url_9);
        item_0.setUrl(url_0);

        */

        u1_name=vdb.get("u1_name");
        if(u1_name == VDB_UKN)
            u1_name.clear();

        u2_name=vdb.get("u2_name");
        if(u2_name == VDB_UKN)
            u2_name.clear();

        u3_name=vdb.get("u3_name");
        if(u3_name == VDB_UKN)
            u3_name.clear();

        u4_name=vdb.get("u4_name");
        if(u4_name == VDB_UKN)
            u4_name.clear();

        u5_name=vdb.get("u5_name");
        if(u5_name == VDB_UKN)
            u5_name.clear();

        u6_name=vdb.get("u6_name");
        if(u6_name == VDB_UKN)
            u6_name.clear();

        u7_name=vdb.get("u7_name");
        if(u7_name == VDB_UKN)
            u7_name.clear();

        u8_name=vdb.get("u8_name");
        if(u8_name == VDB_UKN)
            u8_name.clear();

        u9_name=vdb.get("u9_name");
        if(u9_name == VDB_UKN)
            u9_name.clear();

        u0_name=vdb.get("u0_name");
        if(u0_name == VDB_UKN)
            u0_name.clear();

        vdb.close();
    }
}

void Favorite::save()
{
    if(!url_1.startsWith(PRE_HTTP))
        url_1= HTTP + url_1;
    if(!url_2.startsWith(PRE_HTTP))
        url_2= HTTP + url_2;
    if(!url_3.startsWith(PRE_HTTP))
        url_3= HTTP + url_3;
    if(!url_4.startsWith(PRE_HTTP))
        url_4= HTTP + url_4;
    if(!url_5.startsWith(PRE_HTTP))
        url_5= HTTP + url_5;
    if(!url_6.startsWith(PRE_HTTP))
        url_6= HTTP + url_6;
    if(!url_7.startsWith(PRE_HTTP))
        url_7= HTTP + url_7;
    if(!url_8.startsWith(PRE_HTTP))
        url_8= HTTP + url_8;
    if(!url_9.startsWith(PRE_HTTP))
        url_9= HTTP + url_9;
    if(!url_0.startsWith(PRE_HTTP))
        url_0= HTTP + url_0;

    QString file=Env::doc + VDB_FILE;
    std::string fileName=file.toStdString();

    VDB vdb;
    if(vdb.open(fileName.c_str()))
    {
        vdb.put("url_1",url_1);
        vdb.put("url_2",url_2);
        vdb.put("url_3",url_3);
        vdb.put("url_4",url_4);
        vdb.put("url_5",url_5);
        vdb.put("url_6",url_6);
        vdb.put("url_7",url_7);
        vdb.put("url_8",url_8);
        vdb.put("url_9",url_9);
        vdb.put("url_0",url_0);

        vdb.put("u1_name",u1_name);
        vdb.put("u2_name",u2_name);
        vdb.put("u3_name",u3_name);
        vdb.put("u4_name",u4_name);
        vdb.put("u5_name",u5_name);
        vdb.put("u6_name",u6_name);
        vdb.put("u7_name",u7_name);
        vdb.put("u8_name",u8_name);
        vdb.put("u9_name",u9_name);
        vdb.put("u0_name",u0_name);

        vdb.close();
    }
}

void Favorite::open(int index)
{
    switch (index) {
    case 1:
        QDesktopServices::openUrl(QUrl(url_1));
        break;
    case 2:
        QDesktopServices::openUrl(QUrl(url_2));
        break;
    case 3:
        QDesktopServices::openUrl(QUrl(url_3));
        break;
    case 4:
        QDesktopServices::openUrl(QUrl(url_4));
        break;
    case 5:
        QDesktopServices::openUrl(QUrl(url_5));
        break;
    case 6:
        QDesktopServices::openUrl(QUrl(url_6));
        break;
    case 7:
        QDesktopServices::openUrl(QUrl(url_7));
        break;
    case 8:
        QDesktopServices::openUrl(QUrl(url_8));
        break;
    case 9:
        QDesktopServices::openUrl(QUrl(url_9));
        break;
    case 0:
        QDesktopServices::openUrl(QUrl(url_0));
        break;
    }
}
