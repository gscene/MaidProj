﻿#include "codeparser.h"

QString CodeParser::parse(QString &words)
{
    words=words.remove(QRegExp("\\s"));

    if(words.startsWith(P_HTTP) || words.startsWith(P_HTTPS) || words.startsWith(P_MAGNET))
        QDesktopServices::openUrl(QUrl(words));

    else if(words.startsWith(P_YUNPAN) || words.startsWith(P_KUAIPAN))
    {
        words=HTTP_PRE + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.startsWith(P_SHA1))
    {
        words=MAGNET_P + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.length() == 8 || words.length() == 6)
    {
        words=YUNPAN_BASE + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.startsWith("/s/") && words.length() == 11)
    {
        words=YUNPAN_URL + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.length() == 40 || words.length() ==32 )
    {
        words=MAGNET_BASE + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.startsWith("8023") && words.length() == 12)
    {
        words=BAIDU_SEARCH + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else if(words.length() == 12)
    {
        words=KUAIPAN_BASE + words;
        QDesktopServices::openUrl(QUrl(words));
    }

    else
        words=R_ERR;

    return words;
}
