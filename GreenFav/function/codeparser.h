﻿#ifndef CODEPARSER_H
#define CODEPARSER_H

#include"sp_env.h"

#define P_HTTP "http"
#define P_HTTPS "https"
#define P_MAGNET "magnet"
#define P_YUNPAN "pan.baidu"
#define P_KUAIPAN "kuai.xunlei"
#define P_SHA1 "sha1:"

#define MAGNET_BASE "magnet:?xt=urn:btih:"
#define MAGNET_SHA1 "magnet:?xt=urn:sha1:"
#define MAGNET_P "magnet:?xt=urn:"

#define HTTP_PRE "http://"
#define YUNPAN_BASE "http://pan.baidu.com/s/"
#define YUNPAN_URL "http://pan.baidu.com"
#define KUAIPAN_BASE "http://kuai.xunlei.com/d/"
#define BAIDU_SEARCH "http://www.baidu.com/s?wd="

#define R_ERR "error"

class CodeParser
{
public:
   static QString parse(QString &);
};

#endif // CODEPARSER_H
