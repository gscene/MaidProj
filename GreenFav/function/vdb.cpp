﻿#include "vdb.h"

VDB::VDB()
{

}

VDB::~VDB()
{

}

bool VDB::open(const char *fileName)
{
    rc=vedis_open(&pStore,fileName);
    if(rc != VEDIS_OK)
        return false;
    else
        return true;
}

bool VDB::put(QString q_key,  QString q_value)
{
    q_key= "\'" + q_key + "\'";
    q_value = "\'" + q_value + "\'";
    std::string keyStr=q_key.toStdString();
    std::string valueStr=q_value.toStdString();
    cmdStr="SET " + keyStr + " " + valueStr;
    rc=vedis_exec(pStore,cmdStr.c_str(),-1);
    if(rc != VEDIS_OK)
        return false;
    else
        return true;
}

QString VDB::get( QString q_key)
{
    q_key= "\'" + q_key + "\'";
    std::string keyStr=q_key.toStdString();
    cmdStr="GET " + keyStr;
    vedis_exec(pStore,cmdStr.c_str(),-1);
    rc=vedis_exec_result(pStore,&pResult);
    QString q_reply;
    if(rc != VEDIS_OK)
        q_reply=VDB_ERR;
    else
    {
        const char *reply=vedis_value_to_string(pResult,0);
        q_reply=reply;
        if(q_reply.isEmpty())
            q_reply=VDB_UKN;
    }
    return q_reply;
}

bool VDB::del(QString q_key)
{
    std::string keyStr=q_key.toStdString();
    cmdStr="DEL " + keyStr;
    rc=vedis_exec(pStore,cmdStr.c_str(),-1);
    if(rc != VEDIS_OK)
        return false;
    else
        return true;
}

void VDB::close()
{
    vedis_close(pStore);
}
