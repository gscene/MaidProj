#-------------------------------------------------
#
# Project created by QtCreator 2015-06-18T18:49:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = GreenFav
TEMPLATE = app

INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/function
INCLUDEPATH += $$PWD/module
INCLUDEPATH += ../Common

SOURCES += main.cpp\
        facade.cpp \
    t_facade.cpp \
    module/favoriteeditor.cpp \
    module/torus.cpp \
    function/codeparser.cpp \
    ../Common/htshandler.cpp \
    ../Common/htsmodel.cpp \
    ../Common/t_dialog.cpp

HEADERS  += facade.h \
    t_facade.h \
    module/favoriteeditor.h \
    module/torus.h \
    function/codeparser.h \
    function/sp_env.h \
    ../Common/htshandler.h \
    ../Common/htsmodel.h \
    ../Common/t_dialog.h

FORMS    += facade.ui \
    module/favoriteeditor.ui \
    module/torus.ui

RESOURCES += \
    res.qrc

RC_ICONS = ico.ico
