﻿#include "facade.h"
#include "ui_facade.h"

Facade::Facade(QWidget *parent) :
    TransFacade(parent),
    ui(new Ui::Facade)
{
    ui->setupUi(this);

    exp_nml;
    initProps();
}

Facade::~Facade()
{
    delete ui;
}

void Facade::delay(int sec)
{
    time.start();
    while(time.elapsed()<sec*1000)
        qApp->processEvents();
}

void Facade::initProps()
{
    menu=nullptr;
    m_option=nullptr;
    m_favorite=nullptr;

    tor=new Torus(this);
    clipboard=QApplication::clipboard();
    createMenu();
}

void Facade::say(const QString &word, unsigned sec)
{
    curPos=this->pos();
    tor->move(curPos.x() - 120 ,curPos.y() - 100 );
    tor->display(word,sec);
}

void Facade::mouseDoubleClickEvent(QMouseEvent *)
{
    QString clipStr=clipboard->text();

    if(clipStr.isEmpty())
    {
        exp_akd;
        say(QStringLiteral("剪贴板里没有东西！"),6);
        delay(6);
        exp_nml;
        return;
    }

    clipStr=CodeParser::parse(clipStr);
    if(clipStr != R_ERR)
    {
        clipboard->setText(clipStr);
        exp_sml;
        say(QStringLiteral("OK! 任务完成！"));
    }
    else
    {
        exp_akd;
        say(QStringLiteral("无法处理……确定是有效的提取码吗？"));
    }
    delay();
    exp_nml;
}

void Facade::contextMenuEvent(QContextMenuEvent *)
{
    menu->exec(QCursor::pos());
}

void Facade::moveEvent(QMoveEvent *)
{
    curPos=this->pos();

    if(curPos.x() >= d_width - 32 || curPos.x() <= -96)
    {
        moveDefPos();
    }
    else if(curPos.y() >= d_height -32 || curPos.y() <= -96)
    {
        moveDefPos();
    }
}

void Facade::createMenu()
{
    menu=new QMenu(this);
    createFavoriteMenu();

    m_option=menu->addMenu(QStringLiteral("选项"));
    hideAction=m_option->addAction(QStringLiteral("隐藏"));
    moveDefPosAction=m_option->addAction(QStringLiteral("默认位置"));

    quitAction=m_option->addAction(QStringLiteral("退出"));
    connect(quitAction,&QAction::triggered,this,&QApplication::quit);

    connect(hideAction,&QAction::triggered,this,&Facade::detectVisible);
    connect(moveDefPosAction,&QAction::triggered,this,&Facade::moveDefPos);
}

void Facade::createFavoriteMenu()
{
    m_favorite=menu->addMenu(QStringLiteral("收藏夹"));
    editFavoriteAction=m_favorite->addAction(QStringLiteral("编辑..."));
    connect(editFavoriteAction,&QAction::triggered,this,&Facade::editFavoriteMenu);
    m_favorite->addSeparator();

    HTSHandler handler;
    if(handler.load(PATH_DOC + FAV_FILE))
    {
        favoriteHead=handler.getHeadList();
        favoriteMenuItems=handler.getHashTable();

        for(int i=0;i<favoriteHead.size();i++)
        {
            m_favorite->addAction(favoriteHead[i]);
        }
        connect(m_favorite,SIGNAL(triggered(QAction*)),this,SLOT(processFavoriteAction(QAction*)));
    }
}

void Facade::reloadFavoriteMenu()
{
    if(m_favorite == nullptr)
        return;

    m_favorite->clear();
    m_favorite->disconnect();

    editFavoriteAction=m_favorite->addAction(QStringLiteral("编辑..."));
    connect(editFavoriteAction,&QAction::triggered,this,&Facade::editFavoriteMenu);
    m_favorite->addSeparator();

    HTSHandler handler;
    if(handler.load(PATH_DOC + FAV_FILE))
    {
        favoriteHead=handler.getHeadList();
        favoriteMenuItems=handler.getHashTable();

        for(int i=0;i<favoriteHead.size();i++)
        {
            m_favorite->addAction(favoriteHead[i]);
        }
        connect(m_favorite,SIGNAL(triggered(QAction*)),this,SLOT(processFavoriteAction(QAction*)));
        say(QStringLiteral("收藏夹已刷新"));
    }
    else
        say(QStringLiteral("无法加载收藏夹！"));
}

void Facade::editFavoriteMenu()
{
    FavoriteEditor favEditor;
    favEditor.move(400,150);
    if(favEditor.detect(PATH_DOC + FAV_FILE))
    {
        connect(&favEditor,&FavoriteEditor::refresh,this,&Facade::reloadFavoriteMenu);
        favEditor.exec();
    }
    else
        say(QStringLiteral("无法加载收藏夹！"));
}

void Facade::processFavoriteAction(QAction *action)
{
    QString url=favoriteMenuItems.value(action->text());
    QDesktopServices::openUrl(QUrl(url));
}
