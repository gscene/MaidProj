#ifndef TWIDGET_H
#define TWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QPoint>

class TransWidget : public QWidget
{
    Q_OBJECT

public:
    TransWidget(QWidget *parent = 0);

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void detectVisible();

private:
    QPoint winPos;
    QPoint mosPos;
    QPoint dPos;
};

#endif // TWIDGET_H
