﻿#include "t_widget.h"

TransWidget::TransWidget(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_TranslucentBackground);

    setWindowFlags(
                Qt::FramelessWindowHint |
                Qt::Tool |
                Qt::WindowStaysOnTopHint
                );
}

void TransWidget::mousePressEvent(QMouseEvent *event)
{
    winPos=this ->pos();
    mosPos= event ->globalPos();
    dPos=mosPos - winPos;
}

void TransWidget::mouseMoveEvent(QMouseEvent *event)
{
    this->move(event ->globalPos() - dPos );
}

void TransWidget::detectVisible()
{
    if(this->isVisible())
        this->hide();
    else
        this->show();
}
